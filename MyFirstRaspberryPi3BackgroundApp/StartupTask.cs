﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Windows.ApplicationModel.Background;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace MyFirstRaspberryPi3BackgroundApp
{
  public sealed class StartupTask : IBackgroundTask
  {
    private BackgroundTaskDeferral deferral;
    public async void Run(IBackgroundTaskInstance taskInstance)
    {
      deferral = taskInstance.GetDeferral();
      var downloadSandbox = new DownloadWebPageSandbox();
      await downloadSandbox.Download();
      deferral.Complete();
    }
  }
}
