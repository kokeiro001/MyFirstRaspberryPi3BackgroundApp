﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using Windows.Storage;
using System.Diagnostics;

namespace MyFirstRaspberryPi3BackgroundApp
{
  class DownloadWebPageSandbox
  {
    private static readonly string FileSaveDic = @"sandbox_data";
    private static readonly string DownloadUrl = @"https://gist.githubusercontent.com/kokeiro001/870acb2a513c36d3f4cce45ceb94c208/raw/0260c034aec327e204613b8a870cf0d2923b1b77/program_list.md";
    private static readonly string SaveFileName = @"sandbox.txt";

    public DownloadWebPageSandbox()
    {
    }

    public async Task Download()
    {
      Debug.WriteLine("Begin");

      string currentDir = Directory.GetCurrentDirectory();
      Debug.WriteLine("currentDir=" + currentDir);

      string mydoc = ApplicationData.Current.LocalFolder.Path;
      Debug.WriteLine("mydoc=" + mydoc);

      string mkdir = Path.Combine(mydoc, FileSaveDic);
      
      // make directory
      if(!Directory.Exists(mkdir))
      {
        Directory.CreateDirectory(mkdir);
      }

      // download content
      var client = new HttpClient();
      var body = await client.GetStringAsync(DownloadUrl);
      Debug.WriteLine("body=" + body);



      StorageFolder storageFolder =  ApplicationData.Current.LocalFolder;
      StorageFile sampleFile = await storageFolder.CreateFileAsync(
              Path.Combine(FileSaveDic, SaveFileName),
              CreationCollisionOption.ReplaceExisting);

      await FileIO.WriteTextAsync(sampleFile, body);
      Debug.WriteLine("End");
    }
  }
}
