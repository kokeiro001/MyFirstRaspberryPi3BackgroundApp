﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using Windows.ApplicationModel.Background;

using Windows.Storage;
using System.Diagnostics;
using Windows.System.Threading;
using System.IO;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace HogeHogeBackgroundApplication
{
    public sealed class StartupTask : IBackgroundTask
    {
        private static readonly string FileSaveDic = @"hoge.txt";

        BackgroundTaskCancellationReason _cancelReason = BackgroundTaskCancellationReason.Abort;
        volatile bool _cancelRequested = false;
        BackgroundTaskDeferral _deferral = null;
        ThreadPoolTimer _periodicTimer = null;
        float _progress = 0;

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            Debug.WriteLine("Background " + taskInstance.Task.Name + " Starting...");

            _deferral = taskInstance.GetDeferral();

            _periodicTimer = ThreadPoolTimer.CreatePeriodicTimer(new TimerElapsedHandler(PeriodicTimerCallback), TimeSpan.FromSeconds(10));
        }

        //
        // Simulate the background task activity.
        //
        private async void PeriodicTimerCallback(ThreadPoolTimer timer)
        {
            // create file
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = await storageFolder.CreateFileAsync(
                    Path.Combine(FileSaveDic, $"Hoge.txt"),
                    CreationCollisionOption.OpenIfExists);

            Debug.WriteLine(DateTime.Now.ToString());
            await FileIO.AppendTextAsync(sampleFile, $"hoge={DateTime.Now.ToString()}\n");
        }
    }
}
